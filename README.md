# PHISHING - PAGES 

Bienvenue dans le répertoire Git qui contient le code (HTML + CSS) ainsi que les ressources utilisés pour créer les pages qui seront utilisées dans les campagnes de Phishing. 


## ANTIDOTE 

[Page de connexion - Antidote](https://services.druide.com/connexion/externe?app=aw&langue=fr&_ga=2.167199678.1422603507.1684748383-104076563.1684231652)

#### Page originale : 
![Page originale Antidote](https://i.ibb.co/rc703rb/Capture-d-cran-du-2023-05-22-11-40-39.png)

#### Copie de la page : 
![Page copiée Antidote](https://i.ibb.co/FnL1pVy/Capture-d-cran-du-2023-05-22-11-42-15.png)



## BITWARDEN 
 

[Page de connexion - BitWarden](https://vault.bitwarden.com/#/login)

#### Page originale : 

![Page originale BitWarden](https://i.ibb.co/Qr0mM9N/Capture-d-cran-du-2023-05-22-11-43-56.png)

#### Page copiée : 
![Page copiée BitWarden](https://i.ibb.co/7jkf42C/Capture-d-cran-du-2023-05-22-11-44-50.png)


## IRCAD - CLOUD 

[Page de connexion - IRCAD Cloud](https://cloud.ircad.fr/login)

#### Page originale : 

![Page originale IRCAD Cloud](https://i.ibb.co/M149qsL/Capture-d-cran-du-2023-05-22-11-46-26.png)

#### Page copiée : 

![Page copiée IRCAD Cloud](https://i.ibb.co/Qdd2GK5/Capture-d-cran-du-2023-05-22-11-47-51.png)

## IRCAD - INTRANET 

[Page de connexion - IRCAD Intranet](https://intranet.ircad.fr/)

#### Page originale : 
![Page originale - IRCAD Intranet](https://i.ibb.co/gSVb8SQ/Capture-d-cran-du-2023-05-22-11-49-48.png)

#### Page copiée : 

![Page copiée - IRCAD Intranet](https://i.ibb.co/3m5QBSR/Capture-d-cran-du-2023-05-22-11-50-49.png)

## LUCCA 

[Page de connexion - LUCCA](https://www.lucca.fr/login/)

#### Pages originales : 
![Page originale - Lucca](https://i.ibb.co/5B0KZDk/Capture-d-cran-du-2023-05-22-11-52-21.png)

![Tableau de bord - Lucca](https://i.ibb.co/g7frYmh/thumbnail-image001.png)

#### Pages copiées : 

![Page copiée - Lucca](https://i.ibb.co/3FRshpJ/Capture-d-cran-du-2023-05-22-11-54-14.png)

![Tableau de bord copié - Lucca](https://i.ibb.co/kJkTK6h/Capture-d-cran-du-2023-05-22-11-55-20.png)


### NOTES 

Pour ajouter les pages dans Gophish, il faut que le CSS soit contenu dans le HTML.

Exemple : 

    <!DOCTYPE html><html><head>
	<title>Portail</title>
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<style type="text/css">body {font-family: system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;}

	*
	{
	    font-family:system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
	}

	#bg_image 
	{
	    border-radius: 16px;
	    position: relative;
	    margin-top: 40px;
	    margin-left: 40px;
	}

	
	#main_page 
	{
	    display: flex;
		flex-direction: row;
		flex-wrap: nowrap;
		justify-content: center;
		align-items: stretch;
		align-content: stretch;
	}


	#formulaire
	{
	    width: 350px;
	    max-width: 100%;
	    margin: auto;
	    display: flex;
		flex-direction: column;
		flex-wrap: nowrap;
		justify-content: flex-start;
		align-items: stretch;
		align-content: stretch;
	}

	h2 
	{
	    color : #e06029;
	    font-size: 1.25rem;
	    font-family:system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
	}

	input 
	{
	    display:block;
	    box-shadow: 0 0 0 1px #acbbd7;
	    margin-bottom: .5rem;
	    border-radius: .2rem;
	    width: 350px;
	    height: 51.2px;
	    color: #445473;
	    outline: 0;
	}	

	#remember 
	{
	    width: 15px;
	    height: 15px;
	   accent-color: #e06029;
	   border-radius: 16px;
	}

	#box 
	{
	    display: flex;
		flex-direction: row;
		flex-wrap: nowrap;
		justify-content: flex-start;
		align-items: baseline;
		align-content: stretch;
	}


	#lucca_logo
	{
	    max-width: 100%;
	    max-height: 60px;
	    margin-right: 200px;
	    margin-bottom: 20px;
	}


	#login 
	{
	    background-color: #e06029;
	    color:#fafafa;
	    border-radius: 16px;
	    padding: .5rem 1.5rem;
	    font-family:system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
	    font-size: 20px;
	    font-weight: bold;
	}

	#login:hover
	{
	    background-color:  #E67F53;
	}

	p
	{
	    text-align: center;
	    color: #becbe4;
	    font-family:system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
	}

	#entreprise
	{
	    background-color: #fafafa;
	    box-shadow: 0 2px 8px rgba(0,0,0,.04),0 1px 2px rgba(0,0,0,.06);
	    margin-bottom: 1rem;
	    padding-top: .65rem;
	    padding-bottom: .65rem;
	    white-space: normal;
	    text-align: center;
	    font-family:system-ui, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
	    border-radius: 4px;
	    border: 1px solid black;
	    font-size: 18px;
	}

	#entreprise:hover 
	{
	    background-color: #becbe4;
	}

	#links 
	{
	    display: flex;
		flex-direction: row;
		flex-wrap: nowrap;
		justify-content: space-between;
		align-items: stretch;
		align-content: stretch;
	}
	a 
	{
	    color: black;
	    font-size: 12px;
	}
	</style>
	</head>
	<body>
	<div id="main_page">
		<div id="trois_quart_photo"><img id="bg_image" src="https://i.ibb.co/mz9dRGb/lucca.png"/></div>

	<div id="formulaire"><img id="lucca_logo" src="https://svgur.com/i/tCE.svg"/>
		<h2>Bienvenue dans votre espace Lucca</h2>
		<input placeholder="Identifiant"/> <input placeholder="Mot de passe"/>
		<div id="box"><input checked="checked" id="remember" name="remember" type="checkbox"/> <label for="remember">Se souvenir de moi</label></div>
		<button id="login" type="submit">Se connecter -&gt;</button>
		<p>OU</p>
		<button id="entreprise">Connectez vous avec votre compte entreprise</button>
	<div id="links"><a href="lucca.html">Mot de passe oublié ?</a> <a href="lucca.html">Première 	connexion</a></div>
	</div>
	</div>


	</body>
	</html>

